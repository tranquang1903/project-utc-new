<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'parent_id',
        'description',
        'content',
        'thumb',
        'slug',
        'active'
    ];

    /*public function products()
    {
        return $this->hasMany(Product::class, 'menu_id', 'id');
    }*/

    public function products()
    {
        return $this->hasMany(Product::class, 'menu_id', 'product_id');
    }

    public function productAll()
    {
        return $this->belongsToMany(Product::class, 'product_menu', 'menu_id', 'product_id');
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithParentId($query, $parentId)
    {
        return $parentId ? $query->where('parent_id', $parentId) : null;
    }

    public function categories()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'description',
        'content',
        'price',
        'price_sale',
        'active',
        'thumb'
    ];

    /*public function menu()
    {
        return $this->hasOne(Menu::class, 'id', 'menu_id')
            ->withDefault(['name' => '']);
    }*/
    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'product_menu', 'product_id', 'menu_id');
    }

    public function attachMenu($id)
    {
        return $this->menus()->attach($id);
    }

    public function detachMenu()
    {
        return $this->menus()->detach();
    }

    public function syncMenu($id)
    {
        return $this->menus()->sync($id);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithCategoryId($query, $categoryId)
    {
        return $categoryId ? $query->where('category_id', $categoryId) : null;
    }

    public function scopeWithMinPrice($query, $minPrice)
    {
        return $minPrice ? $query->where('price', '>', $minPrice) : null;
    }

    public function scopeWithMaxPrice($query, $maxPrice)
    {
        return $maxPrice ? $query->where('price', '<', $maxPrice) : null;
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Services\Product\User\ProductUserService;
use Illuminate\Http\Request;

class ClientProductController extends Controller
{
    protected $productUserService;

    /**
     * @param $productUserService
     */
    public function __construct(ProductUserService $productUserService)
    {
        $this->productUserService = $productUserService;
    }

    public function index($id = '', $slug = '')
    {
        $product = $this->productUserService->show($id);
        $productsMore = $this->productUserService->more($id);
        return view('user.products.content', [
            'title' => $product->name,
            'product' => $product,
            'products' => $productsMore
        ]);
    }
}

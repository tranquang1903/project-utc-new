<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Services\Menu\MenuService;
use App\Http\Services\Product\User\ProductUserService;
use App\Models\Menu;
use Illuminate\Http\Request;

class ClientMenuController extends Controller
{
    protected $menuService;
    protected $productUserService;

    /**
     * @param $menuService
     */
    public function __construct(MenuService $menuService,  ProductUserService $productUserService)
    {
        $this->menuService = $menuService;
        $this->productUserService = $productUserService;
    }

    public function index(Request $request, $id, $slug = '')
    {
        /*$menu = Menu::where('id', $id)->first();*/
        $menu = $this->menuService->getId($id);

        $products = $this->menuService->getProduct($menu, $request);

        return view('user.products.menu', [
            'title' => $menu->name,
            'products' => $products,
            'menu' => $menu
        ]);
    }


}

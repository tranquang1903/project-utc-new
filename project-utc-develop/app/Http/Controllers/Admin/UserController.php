<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\EditUserRequest;
use App\Http\Services\Role\RoleService;
use App\Http\Services\User\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{

    protected $userService;
    protected $roleService;

    /**
     * @param $userService
     * @param $roleService
     */
    public function __construct(UserService $userService,RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }


    public function create()
    {
        $roles = $this->roleService->all();
        return view('admin.users.add', compact('roles'), [
            'title' => 'Thêm Người Quản Lý'
        ]);
    }

    public function store(CreateUserRequest $request)
    {
        $result = $this->userService->create($request);
        if ($result) {
            return redirect('/admin/users/list')
                ->with('success', 'User create successfully');;
        }
        return redirect()->back();
    }

    public function index(Request $request)
    {
        $userPaginate = $this->userService->search($request);
        return view('admin.users.list', compact('userPaginate'), [
            'title' => 'Danh Sách Người Quản Lý'
        ]);
    }

    public function edit($id)
    {
        $roles = $this->roleService->all();
        $users = $this->userService->all();
        $user = $this->userService->findOrFail($id);
        return view('admin.users.edit',
            ['title' => 'Chỉnh Sửa Người Quản lý'], compact('users', 'user', 'roles'));
    }

    public function update(EditUserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect('/admin/users/list')
            ->with('success', 'User updated successfully');
    }

    public function destroy(Request $request)
    {
        $result = $this->userService->delete($request);
        if($result) {
            return response()->json([
                'error' => false,
                'message' => 'Xóa thành công người quản lý'
            ]);
        }
        return response()->json( ['error' => true ]);

    }
}

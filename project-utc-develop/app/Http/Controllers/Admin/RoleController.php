<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Role\CreateRoleRequest;
use App\Http\Requests\Role\EditRoleRequest;
use App\Http\Services\Role\PermissionService;
use App\Http\Services\Role\RoleService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    protected $roleService;
    protected $permissionService;

    /**
     * @param $roleService
     */
    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    public function create()
    {
        $permissionGroup = $this->permissionService->permissionGroup();
        return view('admin.role.add', compact('permissionGroup'), [
            'title' => 'Thêm Quyền Mới'
        ]);
    }

    public function store(CreateRoleRequest $request)
    {
        $result = $this->roleService->create($request);
        if ($result) {
            return redirect('/admin/roles/list');
        }
        return redirect()->back();

    }

    public function index(Request $request)
    {
        $roles = $this->roleService->search($request);
        $permission = $this->permissionService->permissionGroup();
        return view('admin.role.list', compact('roles', 'permission'), [
        'title' => 'Danh Sách Quyền'
        ]);
    }

    public function edit($id)
    {
        $permissionGroup = $this->permissionService->permissionGroup();
        $role = $this->roleService->findOrFail($id);
        return view('admin.role.edit',
            ['title' => 'Chỉnh Sửa Role'], compact('role', 'permissionGroup'));
    }

    public function update(EditRoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);
        return redirect('/admin/roles/list')
            ->with('success', 'Role updated successfully');
    }

    public function destroy(Request $request)
    {
        $result = $this->roleService->destroy($request);
        if($result) {
            return response()->json([
                'error' => false,
                'message' => 'Xóa thành công Role'
            ]);
        }
        return response()->json( ['error' => true ]);

    }


}

<?php

namespace App\Http\Repositories;

use App\Models\Menu;

class MenuRepository extends BaseRepository
{

    public function model()
    {
        return Menu::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])
            ->latest('id')->paginate(10);
    }
}

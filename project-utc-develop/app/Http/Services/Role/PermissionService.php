<?php

namespace App\Http\Services\Role;

use App\Http\Repositories\PermissionRepository;

class PermissionService
{
    protected $permissionRepository;

    /**
     * @param $permissionRepository
     */
    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function permissionGroup()
    {
        return $this->permissionRepository->all()->groupBy('group_name');
    }


}

<?php

namespace App\Http\Services\Slider;

use App\Http\Repositories\SliderRepository;
use App\Models\Slider;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class SliderService
{

    protected $sliderRepositories;

    /**
     * @param $sliderRepositories
     */
    public function __construct(SliderRepository $sliderRepositories)
    {
        $this->sliderRepositories = $sliderRepositories;
    }


    public function insert($request)
    {
        try {
            Slider::create($request->input());
            Session::flash('success', 'Thêm Slider mới thành công');
        } catch (\Exception $err) {
            Session::flash('error', 'Thêm Slider lỗi');
            \Log::info($err->getMessage());

            return false;
        }
        return true;
    }

    public function get()
    {
        return Slider::orderByDesc('id')->paginate(15);
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        return $this->sliderRepositories->search($dataSearch);
    }

    public function show()
    {
        return Slider::where('active', 1)->orderByDesc('sort_by')->get();
    }

    public function update($request, $slider)
    {
        try {
            $slider->fill($request->input());
            $slider->save();
            Session::flash('success', 'Sửa Slider mới thành công');
        } catch (\Exception $err) {
            Session::flash('error', 'Sửa Slider lỗi');
            \Log::info($err->getMessage());

            return false;
        }
        return true;
    }

    public function destroy($request)
    {
        $slider = Slider::where('id' , $request->input('id'))->first();
        if($slider) {
            $path = str_replace('storage', 'public', $slider->thumb);
            Storage::delete($path);
            $slider->delete();
            return true;
        }

        return false;
    }
}

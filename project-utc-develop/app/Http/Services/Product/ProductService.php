<?php

namespace App\Http\Services\Product;

use App\Http\Repositories\ProductRepository;
use App\Models\Menu;
use App\Models\Product;
use Illuminate\Support\Facades\Session;

class ProductService
{

    protected $productRepository;

    /**
     * @param $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }


    public function getParent()
    {
        return Menu::where('parent_id', 0)->get();

    }

    public function getAll()
    {
        return Menu::orderbyDesc('id')->paginate(10);

    }
    public function getMenu()
    {
        return Menu::where('active', 1)->get();

    }

    public function get()
    {
        /*return Product::with('menu')
                ->orderByDesc('id')->paginate(5);*/
        return Product::orderByDesc('id')->paginate(5);
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['min_price'] = $request->min_price ?? '';
        $dataSearch['max_price'] = $request->max_price ?? '';
        return $this->productRepository->search($dataSearch);
    }

    protected function isValidPrice($request)
    {
        if ($request->input('price') != 0 && $request->input('price_sale') != 0
            && $request->input('price_sale') >= $request->input('price')
        ) {
            Session::flash('error', 'Giá giảm phải nhỏ hơn giá gốc');
            return false;
        }

        if ($request->input('price_sale') != 0 && (int)$request->input('price') == 0) {
            Session::flash('error', 'Vui lòng nhập giá gốc');
            return false;
        }

        return true;
    }

    public function insert($request)
    {
        $isValidPrice = $this->isValidPrice($request);
        if ($isValidPrice === false) return false;
        try {
            $request->except('_token');
            $product =  Product::create($request->all());
            $product->attachMenu($request->menu_id);
            Session::flash('success', 'Thêm Sản Phẩm Thành Công');
        } catch (\Exception $err) {
            Session::flash('success', 'Thêm Sản Phẩm Lỗi');
            \Log::info($err->getMessage());
            return false;
        }
        return true;

    }

    public function update($request, $product)
    {
        $isValidPrice = $this->isValidPrice($request);
        if ($isValidPrice === false) return false;

        try {
            $product->fill($request->input());
            $product->syncMenu($request->menu_id);
            $product->save();
            Session::flash('success', 'Cập nhật thành công');
        } catch (\Exception $err) {
            Session::flash('eror', 'Có lỗi vui lòng thử lại');
            \Log()::infor($err->getMessage());
            return false;
        }
        return true;

    }

    public function delete($request)
    {
        $product = Product::findOrFail($request->id);
        $product->detachMenu();
        return $product->delete();

    }

}

<?php

namespace App\Http\Services\User;

use App\Http\Repositories\UserRepository;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected $userRepositories;

    /**
     * @param $userRepositories
     */
    public function __construct(UserRepository $userRepositories)
    {
        $this->userRepositories = $userRepositories;
    }

    public function all()
    {
        return $this->userRepositories->all();
    }

    public function findOrFail($id)
    {
        return $this->userRepositories->findOrFail($id);
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['email'] = $request->email ?? '';
        return $this->userRepositories->search($dataSearch);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($request->password);
        $user =  $this->userRepositories->create($dataCreate);
        $user->attachRole($request->roles);
        return $user;
    }

    public function update($request, $id)
    {
        $user = $this->userRepositories->findOrFail($id);
        $dataUpdate = $request->except('password');
        if ($request->password) {
            $dataUpdate['password'] = Hash::make($request->password);
        }
        $user->update($dataUpdate);
        $user->syncRole($request->roles);
        return $user;
    }

    public function delete($request)
    {
        $user = User::where('id' , $request->input('id'))->first();
        /*$user = $this->userRepositories->findOrFail($id);*/
        if ($user) {
            $user->detachRole();
            $user->delete();
            return true;
        }

        return false;
    }


}

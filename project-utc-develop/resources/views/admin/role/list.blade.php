@extends('admin.main')

@section('content')
    @php
        $stt = (($_GET['page'] ?? 1) - 1) * 5;
    @endphp

    <div class="col-9 mt-3">
        <form class="form-search" action="{{route('role_list')}}" method="GET" role="form">
            @csrf
            <div class="row">
                <div class="col-3">
                    <input type="text" class="form-control border card" name="name"
                           value="{{$_GET['name'] ?? ''}}"
                           placeholder="Name" style="padding: 0.5rem 1rem !important;">
                </div>

                <div class="col-3">
                    <button type="submit" class="btn btn-outline-info btn-icon"
                            id="btn-search">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th style="width: 50px">NO</th>
            <th>Name</th>
            <th>DisPlay Name</th>
            <th>Active</th>
        </tr>
        </thead>
        <tbody>
        @foreach($roles as $key => $role)
            <tr>
                <th><p style="margin-left: 20px">{{ ++$stt }}</p></th>
                <td>{{ $role->name }}</td>
                <td>{{ $role->display_name}}</td>
                <td>&nbsp;
                    @hasPermission('update-role')
                    <a class="btn btn-primary btn-sm" href="/admin/roles/edit/{{$role->id }}">
                        <i class="fas fa-edit"></i>
                    </a>
                    @endhasPermission

                    @hasPermission('delete-role')
                    <a class="btn btn-danger btn-sm" href="#"
                       onclick="removeRow({{ $role->id }}, '/admin/roles/destroy')">
                        <i class="fas fa-trash"></i>
                    </a>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $roles -> links() }}
@endsection

{{--@section('footer')
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection--}}

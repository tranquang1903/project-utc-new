@extends('admin.main')

@section('content')
    <form action="{{route('update', $role->id)}}" method="POST">
        @method('put')
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="product">Name: </label>
                <input type="text" name="name" value="{{$role->name}}" class="form-control" >
                @error('name')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="product">Display Name: </label>
                <input type="text" name="display_name" value="{{$role->display_name}}" class="form-control" >
                @error('display_name')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>
            <strong class="form-check mb-2">Permission</strong>
            <div class="form-check">
                <div class="form-check mx-3">
                    <input type="checkbox" class="form-check-input check-all">
                    <label style="font-size: 1.1rem;">Check all</label>
                    @foreach($permissionGroup as $group => $permission)
                        {{--                            con 1--}}
                        <div class="card mb-3 col-md-12">
                            <div class="card-header bg-gray-400">
                                <div class="form-check">
                                    <!--                                        <input type="checkbox" class="form-check-input checkbox_wrapper checkbox checkbox-group">-->
                                    <label class="custom-control-label text-dark"
                                    >{{ ucfirst($group) }}</label>
                                </div>
                            </div>
                            <div class="row p-2 pb-0">
                                @foreach($permission as $permissionItem)
                                    {{--                                        con 2--}}
                                    <div class="card-body col-3 ">
                                        <div class="form-check">
                                            <input class="form-check-input p-0 checkbox_children checkbox" type="checkbox"
                                                   name="permission[]"
                                                   @php
                                                       $permissions = $role->permissions
                                                   @endphp
                                                   @foreach($permissions as $item)
                                                   {{ $item->id == $permissionItem->id ? 'checked' : '' }}
                                                   @endforeach
                                                   value="{{ $permissionItem->id }}">
                                            <label class="p-0">
                                                {{ $permissionItem->name }}
                                            </label>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
                <br>

            </div>


        </div>

        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </form>
@endsection
@section('footer')
    <script src="/template/admin/js/role.js"></script>
@endsection

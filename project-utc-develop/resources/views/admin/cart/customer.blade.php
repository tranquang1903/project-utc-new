@extends('admin.main')

@section('content')
    @hasRole('admin')
    <div class="col-9 mt-3">
        <form class="form-search" action="{{--{{route('user_list')}}--}}" method="GET" role="form">
            @csrf
            <div class="row">
                <div class="col-3">
                    <input type="text" class="form-control border card" name="name"
                           value="{{$_GET['name'] ?? ''}}"
                           placeholder="Name" style="padding: 0.5rem 1rem !important;">
                </div>
                <div class="col-3">
                    <input type="text" class="form-control border card" name="email"
                           value="{{$_GET['email'] ?? ''}}"
                           placeholder="Email" style="padding: 0.5rem 1rem !important;">
                </div>
                <div class="col-3">
                    <button type="submit" class="btn btn-info text-white btn-icon"
                            id="btn-search">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th style="width: 50px">ID</th>
            <th>Tên Khách Hàng</th>
            <th>Số Điện Thoại</th>
            <th>Email</th>
            <th>Ngày Đặt</th>
            <th style="width: 200px">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($customers as $key => $customer)
            <tr>
                <td>{{ $customer->id }}</td>
                <td>{{ $customer->name }}</td>
                <td>{{ $customer->phone }}</td>
                <td>{{ $customer->email }}</td>
                <td>{{ $customer->created_at }}</td>
                <td>&nbsp;
                    <a class="btn btn-primary btn-sm" href="/admin/customers/view/{{ $customer->id }}">
                        <i class="fas fa-eye"></i>
                    </a>
                    <a class="btn btn-danger btn-sm" href="#"
                       onclick="removeRow({{ $customer->id }}, '/admin/customers/destroy')">
                        <i class="fas fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $customers->links() !!}

    @endhasRole
@endsection

@section('footer')
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection

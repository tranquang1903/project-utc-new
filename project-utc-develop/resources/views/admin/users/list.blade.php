@extends('admin.main')

@section('content')
    @php
        $stt = (($_GET['page'] ?? 1) - 1) * 5;
    @endphp

    <div class="col-9 mt-3">
        <form class="form-search" action="{{route('user_list')}}" method="GET" role="form">
            @csrf
            <div class="row">
                <div class="col-3">
                    <input type="text" class="form-control border card" name="name"
                           value="{{$_GET['name'] ?? ''}}"
                           placeholder="Name" style="padding: 0.5rem 1rem !important;">
                </div>
                <div class="col-3">
                    <input type="text" class="form-control border card" name="email"
                           value="{{$_GET['email'] ?? ''}}"
                           placeholder="Email" style="padding: 0.5rem 1rem !important;">
                </div>
                <div class="col-3">
                    <button type="submit" class="btn btn-info text-white btn-icon"
                            id="btn-search">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th style="width: 50px">NO</th>
            <th>Name</th>
            <th>Email</th>
            <th>Roles</th>
            <th>Active</th>
        </tr>
        </thead>
        <tbody>
        @foreach($userPaginate as $user)
            <tr>
                <th><p style="margin-left: 20px">{{ ++$stt }}</p></th>
                <th><p>{{$user->name}}</p></th>
                <th><p>{{$user->email}}</p></th>
                <th class="align-middle">
                    @foreach($user->roles as $role)
                        <span
                            class="badge badge-sm bg-gradient-success">{{ $role->display_name }}</span>
                    @endforeach
                </th>
                <th class="align-middle">

                    @hasPermission('update-user')
                    <a class="btn btn-primary btn-sm" href="/admin/users/edit/{{$user->id }}">
                        <i class="fas fa-edit"></i>
                    </a>
                    @endhasPermission

                    @hasPermission('delete-user')
                    {{--@csrf
                    @method('DELETE')--}}
                    <a class="btn btn-danger btn-sm" href="#"
                       onclick="removeRow({{ $user->id }}, '/admin/users/destroy')">
                        <i class="fas fa-trash"></i>
                    @endhasPermission

                    </a>
                </th>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $userPaginate -> links() }}
@endsection

{{--@section('footer')
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection--}}

@extends('admin.main')

@section('content')

    <form id="addCateForm" action="" method="POST"
          accept-charset="UTF-8"
          >
        <div class="card-body">
            <div class="form-group">
                <label for="product">Name: </label>
                <input placeholder="Name" class="form-control" name="name"
                       id="name"
                       type="text"
                       value="{{old('name')}}">
                @error('name')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="product">Email: </label>
                <input placeholder="Email" class="form-control"
                       name="email"
                       type="email"
                       value="{{old('email')}}">
                @error('email')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="product">Password: </label>
                <input placeholder="Password" class="form-control"
                       name="password"
                       type="password">
                @error('password')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="product">Confirm Password: </label>
                <input placeholder="Conform Password" class="form-control"
                       name="password_confirmation"
                       type="password">
                @error('password')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>

            <strong class="form-check mb-2">Roles</strong>

            <div class="form-group ps-4">
<!--                <strong style="display: inline;" class="form-check mb-2">Role: </strong>-->
                <select class="form-select" multiple name="roles[]">
                    @foreach($roles as $item)
                        <option value="{{ $item->id }}">
                            {{ $item->display_name }}
                        </option>
                    @endforeach
                </select>
<!--                <div class="text-danger form-control errors error-roles"></div>-->
            </div>





        </div>

        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tạo Người Quản Lý</button>
        </div>

        @csrf
    </form>
@endsection

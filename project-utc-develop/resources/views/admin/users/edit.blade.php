@extends('admin.main')

@section('content')

    <form id="addCateForm" action="{{route('update', $user->id)}}" method="POST"
          accept-charset="UTF-8"
    >
        @method('put')
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="product">Name: </label>
                <input placeholder="Name" value="{{$user->name}}" class="form-control" name="name"
                       id="name"
                       type="text"
                      >
                @error('name')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="product">Email: </label>
                <input placeholder="Email" class="form-control"
                       name="email"
                       type="email"
                       value="{{$user->email}}">
                @error('email')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="product">Password: </label>
                <input placeholder="Password" class="form-control"
                       name="password"
                       type="password">
                @error('password')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="product">Confirm Password: </label>
                <input placeholder="Conform Password" class="form-control"
                       name="password_confirmation"
                       type="password">
                @error('password')
                <div class="form-check text-danger">{{ $message }}</div>
                @enderror
            </div>

            <strong class="form-check mb-2">Roles</strong>

            <div class="form-group ps-4">
                <!--                <strong style="display: inline;" class="form-check mb-2">Role: </strong>-->
                <select class="form-select" multiple name="roles[]">
                    @foreach($roles as $role)
                        <option @if(isset($user->userRoles))
                                @foreach($user->userRoles as $item)
                                {{$item->id == $role->id ? 'selected' :''}}
                                @endforeach
                                @endif
                                value="{{ $role->id }}">
                            {{ $role->display_name }}
                        </option>
                    @endforeach
                </select>
                @error('roles')
                <span style="display: block"; class="error text-danger">{{$message}}</span>
            @enderror
                <!--                <div class="text-danger form-control errors error-roles"></div>-->
            </div>





        </div>

        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Sửa Người Quản Lý</button>
        </div>

        @csrf
    </form>
@endsection

@extends('admin.main')

@section('content')
    @php
        $stt = (($_GET['page'] ?? 1) - 1) * 5;
    @endphp
    <div class="col-9 mt-3">
        <form class="form-search" action="{{--{{route('menu_list')}}--}}" method="GET" role="form">
            @csrf
            <div class="row">
                <div class="col-3">
                    <input type="text" class="form-control border card" name="name"
                           value="{{$_GET['name'] ?? ''}}"
                           placeholder="Name" style="padding: 0.5rem 1rem !important;">
                </div>
                <div class="col-3">
                    <button type="submit" class="btn btn-info btn-icon"
                            id="btn-search">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
                <div class="col-12 m-2 mb-4">
                    <div class="row">
                        <div class="col-5">
                            <input type="number" class="form-control border card" name="min_price"
                                   value="{{$_GET['min_price'] ?? ''}}" id="min-price"
                                   placeholder="Price min" style="padding: 0.5rem 1rem !important;">
                        </div>
                        <span class="mt-2" style="width: 0px;font-weight: bold; padding: 0">-</span>
                        <div class="col-5">
                            <input type="number" class="form-control border card" name="max_price"
                                   value="{{$_GET['max_price'] ?? ''}}" id="max-price"
                                   placeholder="Price max" style="padding: 0.5rem 1rem !important;">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th style="width: 70px">ID</th>
            <th>Tên Sản Phẩm</th>
            <th>Danh Mục</th>
            <th>Giá Gốc</th>
            <th>Giá Khuyến Mãi</th>
            <th>Ảnh</th>
            <th>Active</th>
            <th>Update</th>
            <th style="width: 200px">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $key => $product)
            <tr>
                <td><p style="margin-left: 20px">{{ ++$stt }}</p></td>
                <td>{{ $product->name }}</td>
                <td>
                    @foreach($product->menus as $item )
                        {{$item->name}},
                    @endforeach
                </td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->price_sale }}</td>
                <td>
                    <div id="image_show">
                        <a href="{{$product->thumb}}" target="_blank">
                            <img src="{{$product->thumb}}" width="100px">
                        </a>
                    </div>
                </td>

                <td>{!! \App\Helpers\Helper::active($product->active) !!}</td>
                <td>{{ $product->updated_at }}</td>
                <td>&nbsp;
                    @hasPermission('update-product')
                    <a class="btn btn-primary btn-sm" href="/admin/products/edit/{{ $product->id }}">
                        <i class="fas fa-edit"></i>
                    </a>
                    @endhasPermission

                    @hasPermission('delete-product')
                    <a class="btn btn-danger btn-sm" href="#"
                       onclick="removeRow({{ $product->id }}, '/admin/products/destroy')">
                        <i class="fas fa-trash"></i>
                    </a>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $products->links() !!}
@endsection

@section('footer')
    <script>
        CKEDITOR.replace('content');
    </script>
@endsection

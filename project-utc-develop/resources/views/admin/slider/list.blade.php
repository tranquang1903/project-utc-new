@extends('admin.main')

@section('content')
    @php
        $stt = (($_GET['page'] ?? 1) - 1) * 5;
    @endphp
    <div class="col-9 mt-3">
        <form class="form-search" action="{{route('slider_list')}}" method="GET" role="form">
            @csrf
            <div class="row">
                <div class="col-3">
                    <input type="text" class="form-control border card" name="name"
                           value="{{$_GET['name'] ?? ''}}"
                           placeholder="Name" style="padding: 0.5rem 1rem !important;">
                </div>
                <div class="col-3">
                    <button type="submit" class="btn btn-info btn-icon"
                            id="btn-search">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th style="width: 50px">ID</th>
            <th>Tiêu Đề</th>
            <th>Link</th>
            <th>Ảnh</th>
            <th>Trạng Thái</th>
            <th>Cập Nhật</th>
            <th style="width: 200px">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sliders as $key => $slider)
            <tr>
                <td><p style="margin-left: 20px">{{ ++$stt }}</p></td>
                <td>{{ $slider->name }}</td>
                <td>{{ $slider->url}}</td>
                <td>
                    <div id="image_show">
                        <a href="{{$slider->thumb}}" target="_blank">
                            <img src="{{$slider->thumb}}" width="100px">
                        </a>
                    </div>
                </td>

                <td>{!! \App\Helpers\Helper::active($slider->active) !!}</td>
                <td>{{ $slider->updated_at }}</td>
                <td>&nbsp;
                    @hasPermission('update-slider')
                    <a class="btn btn-primary btn-sm" href="/admin/sliders/edit/{{ $slider->id }}">
                        <i class="fas fa-edit"></i>
                    </a>
                    @endhasPermission

                    @hasPermission('delete-slider')
                    <a class="btn btn-danger btn-sm" href="#"
                       onclick="removeRow({{ $slider->id }}, '/admin/sliders/destroy')">
                        <i class="fas fa-trash"></i>
                    </a>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $sliders->links() !!}
@endsection


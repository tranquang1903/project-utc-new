<?php

use App\Http\Controllers\Admin\CartController;
use App\Http\Controllers\Admin\MainController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\Users\LoginController;
use App\Http\Controllers\User\ClientCartController;
use App\Http\Controllers\User\ClientMainController;
use App\Http\Controllers\User\ClientMenuController;
use App\Http\Controllers\User\ClientProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/users/login', [LoginController::class, 'index'])->name('admin.login');
Route::post('admin/users/login/store', [LoginController::class, 'store']);

Route::middleware(['auth'])->group(function (){

    Route::prefix('admin')->group(function () {
        Route::get('/', [MainController::class, 'index'])->name('admin')
            ;
        Route::get('main', [MainController::class, 'index']);
        Route::get('logout', [LoginController::class, 'logout']);

        #Menu
        Route::prefix('menus')->group(function () {
            Route::get('add', [MenuController::class, 'create'])
                ->middleware('check.permission:create-category');
            Route::post('add', [MenuController::class, 'store'])
                ->middleware('check.permission:create-category');
            Route::get('list', [MenuController::class, 'index'])->name('menu_list');;
            Route::get('edit/{menu}', [MenuController::class, 'show'])
                ->middleware('check.permission:update-category');
            Route::post('edit/{menu}', [MenuController::class, 'update'])
                ->middleware('check.permission:update-category');
            Route::delete('destroy', [MenuController::class, 'destroy'])
                ->middleware('check.permission:delete-category');

        });

        #Product
        Route::prefix('products')->group(function () {
            Route::get('add', [ProductController::class, 'create']);
            Route::post('add', [ProductController::class, 'store'])
                ->middleware('check.permission:create-product');
            Route::get('list', [ProductController::class, 'index']);
            Route::get('edit/{product}', [ProductController::class, 'show'])
                ->middleware('check.permission:show-product');
            Route::post('edit/{product}', [ProductController::class, 'update'])
                ->middleware('check.permission:update-product');
            Route::DELETE('destroy', [ProductController::class, 'destroy'])
                ->middleware('check.permission:delete-product');
        });

        #Slider
        Route::prefix('sliders')->group(function () {
            Route::get('add', [SliderController::class, 'create']);
            Route::post('add', [SliderController::class, 'store'])
                ->middleware('check.permission:create-slider');
            Route::get('list', [SliderController::class, 'index'])->name('slider_list');
            Route::get('edit/{slider}', [SliderController::class, 'show'])
                ->middleware('check.permission:show-slider');
            Route::post('edit/{slider}', [SliderController::class, 'update'])
                ->middleware('check.permission:update-slider');
            Route::DELETE('destroy', [SliderController::class, 'destroy'])
                ->middleware('check.permission:delete-slider');
        });

        #Role
        Route::prefix('roles')->group(function () {
            Route::get('add', [RoleController::class, 'create']);
            Route::post('add', [RoleController::class, 'store'])
                ->middleware('check.permission:create-role');
            Route::get('list', [RoleController::class, 'index'])->name('role_list');
            Route::get('edit/{role}', [RoleController::class, 'edit'])->name('edit')
                ->middleware('check.permission:update-role');
            Route::put('/{role}', [RoleController::class, 'update'])->name('update')
                ->middleware('check.permission:update-role');
            Route::DELETE('destroy', [RoleController::class, 'destroy'])
                ->middleware('check.permission:delete-role');
        });

        #User in Admin
        Route::prefix('users')->group(function () {
            Route::get('add', [UserController::class, 'create']);
            Route::post('add', [UserController::class, 'store'])
                ->middleware('check.permission:create-user');
            Route::get('list', [UserController::class, 'index'])->name('user_list');
            Route::get('edit/{user}', [UserController::class, 'edit'])->name('edit')
                ->middleware('check.permission:update-user');
            Route::put('/{user}', [UserController::class, 'update'])->name('update')
                ->middleware('check.permission:update-user');
            Route::DELETE('destroy', [UserController::class, 'destroy'])
                ->middleware('check.permission:delete-user');
        });


        #Upload
        Route::post('upload/services', [UploadController::class, 'store']);

        #Cart
        Route::get('customers', [CartController::class, 'index']);
        Route::get('customers/view/{customer}', [CartController::class, 'show'])
            ->middleware('check.role:admin');
    });
});

Route::get('/', [ClientMainController::class, 'index']);
Route::post('/services/load-products', [ClientMainController::class, 'loadProduct']);

/*lay ra danh sach san pham*/
Route::get('danh-muc/{id}-{slug}.html', [ClientMenuController::class, 'index']);

/*hien ra chi tiet san pham*/
Route::get('san-pham/{id}-{slug}.html', [ClientProductController::class, 'index']);

/*them gio hang*/
Route::post('/add-cart', [ClientCartController::class, 'index']);
Route::get('carts', [ClientCartController::class, 'show']);
Route::post('update-cart', [ClientCartController::class, 'update']);
Route::get('carts/delete/{id}', [ClientCartController::class, 'remove']);
Route::post('carts', [ClientCartController::class, 'addCart']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

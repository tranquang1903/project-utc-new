$(document).ready(function () {
    $('.check-all').on('change', function () {
        $('.checkbox').prop('checked', $(this).prop("checked"));
    });

    if ($('.checkbox:checked').length == $('.checkbox').length) {
        $('.select_all').prop('checked', true);
    }

    $('.checkbox').change(function () {
        if ($('.checkbox:checked').length == $('.checkbox').length) {
            $('.check-all').prop('checked', true);
        } else {
            $('.check-all').prop('checked', false);
        }
    });
});
